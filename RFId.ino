#include "SoftwareSerial.h"
#include "Servo.h"
#define LENGTH_OF_ID 10
#define RFID_RX 10
#define RFID_TX 11
#define RFID_ENABLE_LOW_PIN 8
#define RFID_BAUD_RATE 2400

//0.9ms min to 2.1ms max (1.5 center) //900 to 2100 microseconds
#define SERVO_PIN 9
#define SERVO_MIN_POS 1000
#define SERVO_MAX_POS 2000

//Software serial for RFID reader
SoftwareSerial rfid(RFID_RX,RFID_TX); //10-RX, 11-TX

String id_read;
String round_tag_id = "0415DB29B9"; //Numbers read from the round RFID tag
String rect_tag_id = "0F03028C25"; //Numbers read from the rectangular RFID tag
int charsRead = 0; //Counter
char temp_char;
Servo servo;

void setup()
{ 
  //Initialize serial for serial monitor
  Serial.begin(RFID_BAUD_RATE);
  
  //Set pin 8 to low to enable the RFID reader
  pinMode(RFID_ENABLE_LOW_PIN,OUTPUT);
  digitalWrite(RFID_ENABLE_LOW_PIN,LOW);
  
  //Set indicator light to high for debugging purposes
  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);
  
  //Set the software serial
  rfid.begin(RFID_BAUD_RATE);
  
  //Start servo
  servo.attach(SERVO_PIN, SERVO_MIN_POS, SERVO_MAX_POS);
  servo.write(0); //Start at 0 degrees
}

void loop()
{
  //Card should output 12-bit ASCII, starting with 0x0A
  //and ending with 0x0D
  charsRead = rfid.available(); //Check to see if an tag was waved
  if (charsRead > 0)
  {
    //Read the ID from buffer
    id_read = ""; //Reset stored ID
    for (unsigned int i = 0; i < LENGTH_OF_ID; ++i)
    {
        temp_char = (char)rfid.read();
        if ((temp_char >= 'A' && temp_char <= 'Z') || (temp_char >= '0' && temp_char <= '9')) id_read += temp_char;
        else if ((int)temp_char == -1) break; //Buffer exhausted
        else --i; //keep trying to read.
        
        delay(10); //Prevent misreads
    }
    
    //Validate ID and start servo
    Serial.print("ID read was: " + id_read + "\n");
    if (id_read.equals(round_tag_id) || id_read.equals(rect_tag_id)) //Valid ID provided
    {
      Serial.println("You are authorized!  Door opening.");
      servo.write(180);
      delay(2000);
      servo.write(0);
      delay(2000); 
    }
    
    //House keeping
    charsRead = 0; //Reset chars read after getting enough digits for a single ID
    delay(3000); //Delay to prevent extra/stray RFID reads
    while (rfid.read() != -1); //Clear buffer  
  }
}

